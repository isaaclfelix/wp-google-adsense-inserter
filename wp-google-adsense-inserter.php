<?php
/*
Plugin Name: WP Google Adsense Inserter
Plugin URI:
Description: A plugin to insert google adsense script tags to the site header and the start of the body
Version: 1.0
Author: Isaac L. Félix
Author URI:
License: GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: wp-google-adsense-inserter
Domain Path: /languages
*/

// Activation Hook
function wpgai_activation() {

}
register_activation_hook(__FILE__, 'wpgai_activation');

// Deactivation Hook
function wpgai_deactivation() {

}
register_deactivation_hook(__FILE__, 'wpgai_deactivation');

function wpgai_add_header_script() {
  // $google_ad_client = "ca-pub-8398049867113966";

  // If the google ad client isnt set or set to empty, then do not render script
  $google_ad_client = get_option('wpgai_google_ad_client') !== '' ? get_option('wpgai_google_ad_client') : false;
  // Check if the google ad client is set before rendering script
  if ($google_ad_client) {
  ?>
  <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
  <script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "<?php echo $google_ad_client; ?>",
    enable_page_level_ads: true
  });
  </script>
  <?php
  }
}
add_action('wp_head', 'wpgai_add_header_script');

require_once('wpgai-settings.php');
?>
