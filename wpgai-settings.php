<?php
require('inputs.php');

// Add the menu page
function wpgai_add_menu_page() {
  add_menu_page('WP Google Adsense Inserter', 'WPGAI', 'manage_options', 'wpgai-settings', 'do_wpgai_settings_menu_page', null, 99);
}
add_action('admin_menu', 'wpgai_add_menu_page');

// Menu page display callback
function do_wpgai_settings_menu_page() {
  ?>
  <div class="wrap" id="wpgai-settings">
    <h1>Wordpress Google Adsense Inserter</h1>
    <?php $active_tab = isset($_GET['tab']) ? $_GET['tab'] : 'general_options'; ?>
    <h2 class="nav-tab-wrapper mb-4">
      <a href="?page=wpgai-settings&tab=general_options" class="nav-tab <?php echo $active_tab == 'general_options' ? 'nav-tab-active' : ''; ?>">General</a>
    </h2>
    <form method="post" action="options.php">
      <?php
      if($active_tab == 'general_options') {
        settings_fields('wpgai-general-options');
        do_settings_sections('wpgai-general-options');
      }
      submit_button();
      ?>
    </form>
  </div>
  <?php
}

// Add settings
function wpgai_settings() {
  add_settings_section('wpgai_general_options', 'General Options', null, 'wpgai-general-options');
  add_settings_field('wpgai_google_ad_client', 'Google Ad Client', 'wpgai_inputTypeText', 'wpgai-general-options', 'wpgai_general_options', array('option_name' => 'wpgai_google_ad_client'));
  register_setting('wpgai-general-options', 'wpgai_google_ad_client');
}
add_action('admin_init', 'wpgai_settings');

?>
