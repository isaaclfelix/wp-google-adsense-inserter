<?php
function wpgai_inputTypeText($args) {
  if (!isset($args['option_name'])) return;
  ?>
  <input type="text" name="<?php echo $args['option_name']; ?>" value="<?php echo esc_attr(get_option($args['option_name'], '')); ?>">
  <?php
}
?>
